<?php
// $Id: subuser.module,v 1.7 2009/10/15 07:45:29 boombatower Exp $
/**
 * @file
 * Allows users of a particular role to create sub user account in another role.
 *
 * Copyright 2008-2009 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/*
 * Variables loaded as constants.
 */
define('SUBUSER_PARENT', variable_get('subuser_parent', 'Parent'));
define('SUBUSER_LIST', variable_get('subuser_list', 'Subusers'));
define('SUBUSER_CREATE', variable_get('subuser_create', 'Create subuser'));
define('SUBUSER_ADMINISTER', variable_get('subuser_administer', 'Administer subusers'));

/**
 * Implements hook_menu().
 */
function subuser_menu() {
  $items = array();

  $items['admin/config/subuser'] = array(
    'title' => 'Subuser',
    'description' => 'Define what, if any, roles are assigned to a new subuser.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('subuser_settings_form'),
    'access arguments' => array('administer subuser settings'),
    'file' => 'subuser.pages.inc'
  );
  $items['user/%user/subuser/create'] = array(
    'title' => variable_get('subuser_create', 'Create subuser'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('subuser_create_form', 1),
    'access arguments' => array('create subuser'),
    'type' => MENU_CALLBACK,
    'file' => 'subuser.pages.inc'
  );
  $items['subuser/switch/%'] = array(
    'title' => 'Switch subuser',
    'page callback' => 'subuser_switch_user',
    'page arguments' => array(2),
    'access callback' => 'subuser_switch_user_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function subuser_permission() {
  return array(
    'create subuser' => array(
      'title' => t('create subuser'),
      'description' => t('TODO Add a description for \'create subuser\''),
    ),
    'switch subuser' => array(
      'title' => t('switch subuser'),
      'description' => t('TODO Add a description for \'switch subuser\''),
    ),
    'administer subuser settings' => array(
      'title' => t('administer subuser settings'),
      'description' => t('TODO Add a description for \'administer subuser settings\''),
    ),
    'administer subusers' => array(
      'title' => t('administer subusers'),
      'description' => t('TODO Add a description for \'administer subusers\''),
    ),
  );
}

/**
 * Implements hook_menu_link_alter().
 *
 * Allow the logout link to be altered.
 *
 * @see subuser_translated_menu_link_alter()
 */
function subuser_menu_link_alter(&$item) {
  if ($item['link_path'] == 'user/logout') {
    $item['options']['alter'] = TRUE;
  }
}

/**
 * Implements hook_translated_menu_link_alter().
 *
 * If currently running as a child user change the "Log out" link to
 * "Log out (return)".
 */
function subuser_translated_menu_link_alter(&$item, $map) {
  if ($item['href'] == 'user/logout' && isset($_SESSION['subuser_uid'])) {
    $item['title'] = t('Log out (return)');
    $item['href'] = 'subuser/switch/' . $_SESSION['subuser_uid'];
  }
}

/**
 * Implements hook_menu_link_alter().
 */
function subuser_menu_alter(&$items) {
 
 // these access callbacks cause problems with the menu system:
 // Missing argument 2 for user_category_load(), missing argument 3 for user_category_load()
 // commenting, will turn back on when the rest of the module functionality has been vetted.
 //  $items['user/%user_category/edit']['access callback'] = 'subuser_user_edit_access';
 //  $items['admin/user/user']['access callback'] = 'subuser_administer_users_access';
 //  $items['admin/user/user']['title callback'] = 'subuser_administer_users_title';
}

/**
 * Modified version of user_edit_access() for 'administer subusers' logic.
 */
function subuser_user_edit_access($account) {
  return true;
  // Condition from user_edit_access().

  if ((($GLOBALS['user']->uid == $account->uid) || user_access('administer users')) && $account->uid > 0) {
    return TRUE;
  }

  // Check if user can administer subusers and the user being editted is a
  // subuser of the active user.
  $is_child = FALSE;
  $is_child = db_select('user_relationship', 'ur')->fields('ur', array('uid'))->condition('parent_id', $account->uid)->execute()->fetchField();
  dpm($is_child);
  /*
  db_select('SELECT uid FROM {user_relationship} WHERE uid = :uid AND parent_id = :parent_id', array(':uid' => $account->uid, ':parent_id' => $GLOBALS['user']->uid))->fetchField()
  */

  if (user_access('administer subusers') && $is_child) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Access callback for admin/user/user page.
 *
 * If user has 'administer users' or 'administer subusers' then allow them to
 * view the administer users page. Filter the view to only the user's they are
 * a parent of if they do not have the 'administer users' permission.
 *
 * @return boolean TRUE access granted, otherwise FALSE.
 */
function subuser_administer_users_access() {
  return TRUE;
  if (user_access('administer users')) {
    return TRUE;
  }

  if (user_access('administer subusers')) {
    global $user;

    if (!isset($_SESSION['user_overview_filter'])) {
      $_SESSION['user_overview_filter'] = array();
    }

    // Look for the subuser filter and ensure it is set to the current user if
    // found, otherwise it will be added bellow.
    $found = FALSE;
    foreach ($_SESSION['user_overview_filter'] as $index => $filter) {
      list($key, $value) = $filter;

      if ($key == 'subuser') {
        $_SESSION['user_overview_filter'][$index][1] = $user->uid;
        $found = TRUE;
        break;
      }
    }

    // Explicitly add the filter.
    if (!$found) {
      $_SESSION['user_overview_filter'][] = array(
        'subuser',
        $user->uid,
      );
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Title callback for admin/user/user page.
 *
 * Set the title to the custom subuser administer title when user has
 * 'administer subusers' and not 'administer users' permission.
 *
 * @return string Either default title or custom subuser title.
 */
function subuser_administer_users_title() {
  if (!user_access('administer users') && user_access('administer subusers')) {
    return t(SUBUSER_ADMINISTER);
  }
  return t('Users');
}

/**
 * Check if the user has permission to switch the specified user.
 *
 * Pass cases:
 *   - Super user.
 *   - Returning to parent account.
 *   - The user is a parent of the user being switched to.
 *
 * @param integer $uid User ID being switched to.
 * @return boolean Access granted.
 */
function subuser_switch_user_access($uid) {
  return true;
  global $user;

  if ($user->uid == 1 ||
      (isset($_SESSION['subuser_uid']) && $uid == $_SESSION['subuser_uid']) ||
      db_query('SELECT uid FROM {user_relationship} WHERE uid = :uid AND parent_id = :parent_id', array(':uid' => $uid, ':parent_id' => $user->uid))->fetchField()) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Switch from a parent user to a subuser (or child user).
 *
 * @param $uid The user id to switch to.
 */
function subuser_switch_user($uid) {
  global $user;

  if ($uid) {
    $_SESSION['subuser_uid'] = ((isset($_SESSION['subuser_uid']) && $uid == $_SESSION['subuser_uid']) ? NULL : $user->uid);
    // TODO Convert "user_load" to "user_load_multiple" if "$uid" is other than a uid.
    // To return a single user object, wrap "user_load_multiple" with "array_shift" or equivalent.
    // Example: array_shift(user_load_multiple(array(), $uid))
    $user = user_load($uid);
  }
  drupal_goto('user/' . $uid);
}

/**
 * Implements hook_user_insert().
 */
function subuser_user_insert(&$edit, $account, $category) {
  if (isset($edit['origin']) && $edit['origin'] == 'subuser') {
    // TODO Please review the conversion of this statement to the D7 database API syntax.
    /* db_query('INSERT INTO {user_relationship} (parent_id, uid)
     VALUES (%d, %d)', $edit['parent_user'], $account->uid) */
    $id = db_insert('user_relationship')
  ->fields(array(
      'parent_id' => $edit['parent_user'],
      'uid' => $account->uid,
    ))
  ->execute();
  }
}

/**
 * Implements hook_user_cancel().
 */
function subuser_user_cancel($edit, $account, $method) {
  // TODO Please review the conversion of this statement to the D7 database API syntax.
  /* db_query('DELETE FROM {user_relationship} WHERE uid = %d', $account->uid) */
  db_delete('user_relationship')
  ->condition('uid', $account->uid)
  ->execute();
}

/**
 * Implements hook_user_view().
 */
function subuser_user_view($account, $view_mode) {
  $parent = db_select('user_relationship', 'ur')
    ->fields('ur', array('parent_id'))
    ->condition('uid', $account->uid)->execute()
    ->fetchObject();
  
  if ($parent) {
    // Display link to parent user if available.
    $parent = user_load($parent->parent_id);
    $account->content['subuser_parent'] = array(
          '#type' => 'user_profile_item',
          '#title' => t(SUBUSER_PARENT),
          '#value' => theme('username', array('account' => $parent)),
          '#weight' => 10,
        );
  }

  // The parent user should either have access to create subusers, or have
  // existing subusers.
  $create = user_access('create subuser');
  $administer = user_access('administer subusers');
  $view = views_get_view('subusers');
  if ($create || (isset($view->results) && $view->results)) {
    $view = views_embed_view('subusers');

    if ($create) {
      $links[] = l(t(SUBUSER_CREATE), 'user/' . $account->uid . '/subuser/create');
    }
    if ($administer) {
      $links[] = l(t(SUBUSER_ADMINISTER), 'admin/user/user');
    }

    $output = implode(' | ', $links);
    $output .= '<br />' . $view;


    $account->content['subuser'] = array(
          '#type' => 'user_profile_category',
          '#title' => t(SUBUSER_LIST),
          '#weight' => 11,
        );
    $account->content['subuser']['list'] = array(
          '#type' => 'user_profile_item',
          '#markup' => $output,
          '#weight' => 11,
        );
  }
}

/**
 * Implements hook_user().
 */
function subuser_user_OLD($op, &$edit, &$account, $category = NULL) { }

/**
 * Implements hook_views_api().
 */
function subuser_views_api() {
  return array(
    'api' => 2,
  );
}

/**
 * Implements hook_views_data().
 */
function subuser_views_data() {
  $data['user_relationship']['table']['group'] = t('Relationship');

  $data['user_relationship']['table']['join']['users'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
  );

  $data['user_relationship']['rid'] = array(
    'title' => t('Relationship: ID'),
    'help' => t('The relationship id'),
  );
  $data['user_relationship']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('The ID of the Sub User.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['user_relationship']['parent_id'] = array(
    'title' => t('Parent Id'),
    'help' => t('The ID of the Parent User.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'uid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
