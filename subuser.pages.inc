<?php
// $Id: subuser.pages.inc,v 1.3 2009/10/14 04:40:34 boombatower Exp $
/**
 * @file
 * Allows users of a particular role to create sub user account in another role.
 *
 * Copyright 2008-2009 by Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Sub user settings form.
 */
function subuser_settings_form($form, &$form_state) {
  $form = array();

  // Get all non-default roles.
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_ANONYMOUS_RID]);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display'),
    '#description' => t('Configure the wording used for creating a subuser.'),
  );
  $form['display']['subuser_parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent'),
    '#description' => t('Name for parent user.'),
    '#default_value' => SUBUSER_PARENT,
  );
  $form['display']['subuser_list'] = array(
    '#type' => 'textfield',
    '#title' => t('List'),
    '#description' => t('Displayed above a users list of subusers.'),
    '#default_value' => SUBUSER_LIST,
  );
  $form['display']['subuser_create'] = array(
    '#type' => 'textfield',
    '#title' => t('Create'),
    '#description' => t('The text used for the link and page title when creating a subuser.'),
    '#default_value' => SUBUSER_CREATE,
  );
  $form['display']['subuser_administer'] = array(
    '#type' => 'textfield',
    '#title' => t('Administer'),
    '#description' => t('The text used for the link and page title when administering subusers.'),
    '#default_value' => SUBUSER_ADMINISTER,
  );

  if ($roles) {
    $form['roles'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles'),
      '#description' => t('Choose the roles you would like for sub users to automatically have when created.')
    );
    $form['roles']['subuser_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Available roles'),
      '#options' => $roles,
      '#default_value' => variable_get('subuser_roles', array()),
    );
  }
  else {
    drupal_set_message(t('No available roles to select. Please <a href="/admin/user/roles">create a role</a>.'), 'error');
  }

  // Ensure that menu rebuild takes place after variables have been saved.
  $form = system_settings_form($form);
  $form['#submit'][] = 'subuser_settings_form_submit';

  return $form;
}

/**
 * Rebuild menu to ensure that display settings take effect.
 */
function subuser_settings_form_submit($form, &$form_state) {
  menu_rebuild();
}

/**
 * Create subuser form.
 *
 * @param object $user User object.
 */
function subuser_create_form($form, &$form_state, $user) {
  $form = array();

  $form['parent_user'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['origin'] = array(
    '#type' => 'value',
    '#value' => 'subuser',
  );
  $form['destination'] = array(
    '#type' => 'hidden',
    '#value' => 'user/' . $user->uid,
  );

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account information'),
    '#weight' => -10,
  );
  $form['account']['status'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );
  $form['account']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => NULL,
    '#maxlength' => 60,
    '#description' => 'Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.',
    '#required' => TRUE,
  );
  $form['account']['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail address'),
    '#default_value' => NULL,
    '#maxlength' => 64,
    '#description' => 'A valid e-mail address. All e-mails from the system will be sent to this address. The e-mail address is not made public and will only be used if you wish to receive a new password or wish to receive certain news or notifications by e-mail.',
    '#required' => TRUE,
  );
  $form['account']['pass'] = array(
    '#type' => 'password_confirm',
    '#description' => 'Provide a password for the new account in both fields.',
    '#required' => TRUE,
    '#size' => 25,
  );
  $form['account']['notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user of new account'),
  );
  $form['roles'] = array(
    '#type' => 'value',
    '#value' => variable_get('subuser_roles', array())
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new account'),
    '#weight' => 30,
  );

  $form['#validate'] = array('user_register_validate');

  return $form;
}

/**
 * Modified copy of user_register_submit().
 *
 * Just changed permission line, and removed uid == 1 case, which should never
 * occur.
 */
function subuser_create_form_submit($form, &$form_state) {
  global $base_url;
  $admin = user_access('create subuser');

  $mail = $form_state['values']['mail'];
  $name = $form_state['values']['name'];
  if (!variable_get('user_email_verification', TRUE) || $admin) {
    $pass = $form_state['values']['pass'];
  }
  else {
    $pass = user_password();
  }
  $notify = isset($form_state['values']['notify']) ? $form_state['values']['notify'] : NULL;
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  if (isset($form_state['values']['roles'])) {
    // Remove unset roles.
    $roles = array_filter($form_state['values']['roles']);
  }
  else {
    $roles = array();
  }

  if (!$admin && array_intersect(array_keys($form_state['values']), array('uid', 'roles', 'init', 'session', 'status'))) {
    watchdog('security', 'Detected malicious attempt to alter protected user fields.', array(), WATCHDOG_WARNING);
    $form_state['redirect'] = 'user/register';
    return;
  }
  // The unset below is needed to prevent these form values from being saved as
  // user data.
  unset($form_state['values']['form_token'], $form_state['values']['submit'], $form_state['values']['op'], $form_state['values']['notify'], $form_state['values']['form_id'], $form_state['values']['affiliates'], $form_state['values']['destination']);

  $merge_data = array('pass' => $pass, 'init' => $mail, 'roles' => $roles);
  if (!$admin) {
    // Set the user's status because it was not displayed in the form.
    $merge_data['status'] = variable_get('user_register', 1) == 1;
  }
  $account = user_save('', array_merge($form_state['values'], $merge_data));
  // Terminate if an error occured during user_save().
  if (!$account) {
    drupal_set_message(t("Error saving user account."), 'error');
    $form_state['redirect'] = '';
    return;
  }
  $form_state['user'] = $account;

  watchdog('user', 'New user: %name (%email).', array('%name' => $name, '%email' => $mail), WATCHDOG_NOTICE, l(t('edit'), 'user/' . $account->uid . '/edit'));

  // Add plain text password into user account to generate mail tokens.
  $account->password = $pass;
  if ($admin && !$notify) {
    drupal_set_message(t('Created a new user account for <a href="@url">%name</a>. No e-mail has been sent.', array('@url' => url("user/$account->uid"), '%name' => $account->name)));
  }
  elseif (!variable_get('user_email_verification', TRUE) && $account->status && !$admin) {
    // No e-mail verification is required, create new user account, and login
    // user immediately.
    _user_mail_notify('register_no_approval_required', $account);
    if (user_authenticate($name, $pass)) {
      drupal_set_message(t('Registration successful. You are now logged in.'));
    }
    $form_state['redirect'] = '';
    return;
  }
  elseif ($account->status || $notify) {
    // Create new user account, no administrator approval required.
    $op = $notify ? 'register_admin_created' : 'register_no_approval_required';
    _user_mail_notify($op, $account);
    if ($notify) {
      drupal_set_message(t('Password and further instructions have been e-mailed to the new user <a href="@url">%name</a>.', array('@url' => url("user/$account->uid"), '%name' => $account->name)));
    }
    else {
      drupal_set_message(t('Your password and further instructions have been sent to your e-mail address.'));
      $form_state['redirect'] = '';
      return;
    }
  }
  else {
    // Create new user account, administrator approval required.
    _user_mail_notify('register_pending_approval', $account);
    drupal_set_message(t('Thank you for applying for an account. Your account is currently pending approval by the site administrator.<br />In the meantime, a welcome message with further instructions has been sent to your e-mail address.'));
    $form_state['redirect'] = '';
    return;
  }
}
