$Id: README.txt,v 1.3 2009/04/24 02:23:06 boombatower Exp $

AUTHOR
------
Jimmy Berry ("boombatower", http://drupal.org/user/214218)

PROJECT PAGE
------------
If you need more information, have an issue, or feature request please
visit the project page at: http://drupal.org/project/subuser.

DESCRIPTION
-----------
This module allows users to be given the permission to create subusers. The
subusers may then be automatically assigned a role or roles. The parent of the
subusers then has the ability to manager the users they have created.

To customize the subuser related settings please visit admin/settings/subuser.
Make sure you take a look at the subuser permission available and assign them
appropriately.
