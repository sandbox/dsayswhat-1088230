// $Id: INSTALL.txt,v 1.1 2009/10/14 03:17:09 boombatower Exp $

AUTHOR
------

  Jimmy Berry ("boombatower", http://drupal.org/user/214218)

INSTALLATION
------------

1.  Apply the subuser.patch file to Drupal 6 core.

2.  Enabled subuser.
